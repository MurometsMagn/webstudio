package mainPackage.Company;

public class BadMood implements Mood {
    @Override
    public void penalt(Employee emp, Employee headOfDep) {
        emp.bonus -= 2;
        if (emp != headOfDep) {
            headOfDep.bonus -= 2;
        }
    }

    @Override
    public void bonus(Employee emp, Employee headOfDep) {
        emp.bonus++;
        if (emp != headOfDep)
            headOfDep.bonus++;
    }
}
