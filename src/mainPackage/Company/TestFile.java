package mainPackage.Company;

import java.util.Queue;

public class TestFile {
    public static void main(String[] args) {
        Repo.setImplementation(new TestRepo());
        Company company = new Company();
        for (int i = 0; i < 10; i++) {
            company.addOrder(new Order());
        }

            company.process();


        Visitor visitor = new Visitor();
        visitor.visit(company);

        System.out.println("Company.countOrdersDoneInTime = " + Company.countOrdersDoneInTime);

    /*    //старая версия (см. UML)
        //можно вынести в отдельный метод или в отдельный класс (Visitor)
        company.visit(new IVisitor() {
            @Override
            public void printEmpl(Employee emp) {
                String text = String.format("%1$-15s %2$-15s %3$s %4$d", emp.lastName, emp.firstName,
                        "bonus=", emp.bonus);
                System.out.println("\t" + text);
            }

            @Override
            public void printDepName(Departament departament) {
                System.out.println(departament.depName);
            }

            @Override
            public void printHeadOfDep(Employee emp) {
            }
        });
        */
    }
}
