package mainPackage.Company;

public class GoodMood implements Mood {
    @Override
    public void penalt(Employee emp, Employee headOfDep) {
        emp.bonus--;
        if (emp != headOfDep) {
            headOfDep.bonus--;
        }
    }

    @Override
    public void bonus(Employee emp, Employee headOfDep) {
        emp.bonus += 2;
        if (emp != headOfDep)
            headOfDep.bonus += 2;
    }
}
