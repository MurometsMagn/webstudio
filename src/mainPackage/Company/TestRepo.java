package mainPackage.Company;

public class TestRepo extends Repo { //хранит тестовый набор сотрудников
    Departament backendDep;
    Departament frontendDep;
    Departament designDep;
    Mood goodmood = new GoodMood();
    Mood badmood = new BadMood();

    TestRepo() { //конструктор
        backendDep = new Departament("Backend Department", null, goodmood);
        backendDep.headOfDep = new Employee("Ilon", "Mask");
        backendDep.addEmployee(backendDep.headOfDep);
        backendDep.addEmployee(new Employee("Ivan", "Bush"));
        backendDep.addEmployee(new Employee("Cristy", "Robinson"));
        backendDep.addEmployee(new Employee("Rob", "Kaspersky"));
        backendDep.addEmployee(new Employee("Stiv", "Job"));

        frontendDep = new Departament("Frontend Departament", backendDep, badmood);
        frontendDep.headOfDep = new Employee("Фронт", "Фронтендов");
        frontendDep.addEmployee(frontendDep.headOfDep);
        frontendDep.addEmployee(new Employee("Петя", "Васечкин"));
        frontendDep.addEmployee(new Employee("Вася", "Петров"));

        designDep = new Departament("Design Departament", frontendDep, badmood);
        designDep.headOfDep = new Employee("Jhon", "Djow");
        designDep.addEmployee(designDep.headOfDep); //включается в лист сотрудников
        designDep.addEmployee(new Employee("Иван", "Иванов"));
        designDep.addEmployee(new Employee("Петр", "Петрорв"));
        designDep.addEmployee(new Employee("Василий", "Васильев"));
        designDep.addEmployee(new Employee("Мирон", "Миронов"));
        designDep.addEmployee(new Employee("Артем", "Артемов"));
    }

    @Override
    public Departament getDesignDepartment() {
        return designDep;
    }

    @Override
    Departament getFrontendDepartment() {
        return frontendDep;
    }

    @Override
    Departament getBackendDepartment() {
        return backendDep;
    }
}
