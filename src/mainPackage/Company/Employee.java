package mainPackage.Company;

import java.sql.ResultSet;
import java.util.*;

public class Employee {
    String firstName;
    String lastName;
    int bonus = 0;
    int wage; //salary
    //Random random = new Random();
    private Queue<Order> orderQueue = new ArrayDeque<>();
    private List<IWorkListener> listeners = new ArrayList<>();

    public Employee(String firstName, String lastName, int bonus, int wage) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.bonus = bonus;
        this.wage = wage;
    }

    public Employee(String firstName, String lastName) {
        this(firstName, lastName, 0, 0);
    }

    public void addOrder(Order order) {
        /**
         * добавить сотр-ку в очередь поставленную задачу
         */

        orderQueue.add(order);
    }

    public boolean hasWork() {
        return !orderQueue.isEmpty();
    }

    public void process() {
        /**
         * брать 1-ю задачу из очереди и выполнять
         *
         * если сделал задачу - возвр задачу
         * если не сделал - возвр нал
         */

        Order order = orderQueue.peek();
        if (order == null) throw new IllegalStateException("orderQueue is empty");

        double rnd = (Math.random() * (95 - 70)) + 70; //=70-95% -вероятность выполнения в срок
        boolean done = Math.random() * 100 < rnd; //выполнил — не выполнил
        order.doneInTime++;
        if (done) {
            orderQueue.poll();

            for (IWorkListener listener : listeners) {
                listener.onWorkReady(this, order);
            }
        }
    }

    public void addListener(IWorkListener workListener) {
        listeners.add(workListener);
    }
}
