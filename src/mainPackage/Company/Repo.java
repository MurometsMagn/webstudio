package mainPackage.Company;

public abstract class Repo {
    abstract Departament getDesignDepartment();
    abstract Departament getFrontendDepartment();
    abstract Departament getBackendDepartment();

    private static Repo implementation = null;

    public static Repo getImplementation() {
        return implementation;
    }

    public static void setImplementation(Repo repo) {
        implementation = repo;
    }
}
