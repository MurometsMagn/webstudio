package mainPackage.Company;

public class Order {
    /**
     * необх доп-ть класс одер новыми св-ми, вл-щими на успех сотр-ка
     * напр или сложность задачи или награду за задачу
     */

    private static int maxOrderCounter = 0; //для увеличения orderCounter
    private int orderCounter;
    private String orderTitle;
    int doneInTime = 0; //0- не сделал, 1-сделал с первого раза, 2 и более - просрочил

    public Order() {
        maxOrderCounter++;
        orderCounter = maxOrderCounter;
        orderTitle = ("Номер заказа: " + orderCounter );
        doneInTime = 0;
       // System.out.print(orderTitle);
    }

    public String getOrderTitle() {
        return orderTitle;
    }
}
