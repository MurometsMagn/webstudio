package mainPackage.Company;

import java.util.Queue;

/**
 * Created by Smolentsev Il'ya on 10.02.2020.
 */
public class Visitor implements IVisitor{ //Visitor занимается только распечаткой в консоль
    @Override
    public void printEmpl(Employee emp) {
        String text = String.format("%1$-15s %2$-15s %3$s %4$d", emp.lastName, emp.firstName,
                "bonus=", emp.bonus);
        System.out.println("\t" + text);
    }

    @Override
    public void printDepName(Departament departament) {
        System.out.println(departament.depName);
    }

    @Override
    public void printHeadOfDep(Employee employee) {
        System.out.println("Head of Departament: " + employee.firstName
                + " " + employee.lastName);

    }

    public void visit(Queue<Employee> employees) {
        for (Employee emp : employees) {
            printEmpl(emp);
        }
    }

    public void visit(Company company) {
        printDepName(company.designDep);
        printHeadOfDep(company.designDep.headOfDep);
        visit(company.designDep.employees);

        printDepName(company.frontendDep);
        printHeadOfDep(company.frontendDep.headOfDep);
        visit(company.frontendDep.employees);

        printDepName(company.backendDep);
        printHeadOfDep(company.backendDep.headOfDep);
        visit(company.backendDep.employees);
    }
}
