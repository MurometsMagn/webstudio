package mainPackage.Company;

public class Company {
    Departament designDep = Repo.getImplementation().getDesignDepartment();
    Departament frontendDep = Repo.getImplementation().getFrontendDepartment();
    Departament backendDep = Repo.getImplementation().getBackendDepartment();

    static int countOrdersDoneInTime; //нигде не реализовано

    public void addOrder(Order order) {
        designDep.addOrder(order);
    }

    public void process() {
        boolean flag; //локальный флажок
        do {
            flag = false;
            flag = designDep.process() || flag;
            flag = frontendDep.process() || flag;
            flag = backendDep.process() || flag;
        } while (flag);
    }

    //убрал в класс Visitor
    public void visit(IVisitor visitor) {
        visitor.printDepName(designDep);
        designDep.visit(visitor);

        visitor.printDepName(frontendDep);
        frontendDep.visit(visitor);

        visitor.printDepName(backendDep);
        backendDep.visit(visitor);
    }

}
