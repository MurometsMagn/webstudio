package mainPackage.Company;

/**
 * Created by Smolentsev Il'ya on 01.03.2020.
 */
interface IWorkListener {
    void onWorkReady(Employee employee, Order order);
}
