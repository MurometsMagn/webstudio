package mainPackage.Company;

import java.util.*;

/*
Добавление элементов в очередь делается методом add(),
 удаление - poll(),
  получение первого элемента без его удаления - peek().
  Стек:
  Добавление элементов осуществляется методом push(),
   а удаление методом pop().
 */

public class Departament implements IWorkListener {
    Queue<Employee> employees = new ArrayDeque<>(); //двусвязанная очередь, включает и начальника отдела
    Employee headOfDep;
    String depName;
    private Departament nextDep;
    Mood mood;

    public Departament(String depName, Departament nextDep, Mood mood) {
        this.depName = depName;
        this.nextDep = nextDep;
        this.mood = mood;
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
        employee.addListener(this);
    }

    public void addOrder(Order order) {
        /**
         * взять сотр-ка, вызвать у него метод addOrder
         * поместить сотр-ка в конец очереди
         */
        Employee emp;
        emp = employees.poll();//взять в начале с удалением

        emp.addOrder(order);//?

        employees.add(emp);//вставить в конец
    }

    public boolean process() {
        /**
         * у кажд сотр-ка вызывает метод процесс
         */
        boolean flag = false; //anyone has a task
        for (Employee emp : employees) {
            if (!emp.hasWork()) continue; //если у работника нет задач, то берем сл.работника
            flag = true;

            Order order;
            emp.process();
        }
        return flag;
    }

    public void visit(IVisitor visitor) {
        for (Employee emp : employees) {
            visitor.printEmpl(emp);
        }
    }

    @Override
    public void onWorkReady(Employee emp, Order order) {
        if (order.doneInTime == 1) {
            mood.bonus(emp, headOfDep);
            Company.countOrdersDoneInTime++;
        } else if (order.doneInTime >= 2) {
            mood.penalt(emp, headOfDep);
        } else throw new IllegalStateException
                ("никто ниче не делал.." + " order.doneInTime" + order.doneInTime);

        if (nextDep != null) {
            order.doneInTime = 0;
            nextDep.addOrder(order);
            //подумать с третьего раза
            //doneInTime = nextDep.process() && doneInTime;
        }
    }
}
