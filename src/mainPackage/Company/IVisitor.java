package mainPackage.Company;

public interface IVisitor {
    void printEmpl(Employee employee); //выводит на печать: ФИО, бонус

    void printDepName(Departament departament); //выводит на печать: назв. департамента

    //как реализовать?
    void printHeadOfDep(Employee employee); //выводит на печать: указание что это глава отдела
}
