package mainPackage.old.departaments;

import mainPackage.old.order.Order;
import mainPackage.old.staff.HeadOfDep;
import mainPackage.old.staff.Imployee;
import mainPackage.old.staff.OrdinaryEmployee;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Smolentsev Il'ya on 24.01.2020.
 */
public abstract class Departament {
    List<Imployee> imployees = new LinkedList(); //будет ли этот лист - один на все деп-ты, или в каждом свой?
    Order order;
    int maxStaff = 3; //максимальное число рядовых сотрудников в отделе
    String depTitle;

    public Departament(String depTitle) {
        this.depTitle = depTitle;
        imployees.add(new HeadOfDep(depTitle));
        for (int i = 0; i < maxStaff; i++) {
            imployees.add(new OrdinaryEmployee(depTitle, i));
        }
    }



   // public static Departament
}
