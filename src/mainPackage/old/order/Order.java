package mainPackage.old.order;

/**
 * Created by Smolentsev Il'ya on 25.01.2020.
 */
public class Order implements IOrder {
    private static int maxOrderCounter = 0;
    private int orderCounter;

    private String orderTitle;

    public Order() {
        maxOrderCounter++;
        orderCounter = maxOrderCounter; //не увеличиваецо
        orderTitle = ("Номер заказа: " + orderCounter );
        System.out.println(orderTitle);
    }

    public int getOrderCounter() {
        return orderCounter;
    }

    public String getOrderTitle() {
        return orderTitle;
    }
}
