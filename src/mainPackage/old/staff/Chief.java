package mainPackage.old.staff;

import mainPackage.old.order.Order;

/**
 * Created by Smolentsev Il'ya on 24.01.2020.
 */
public class Chief implements IImployee{ //singleton
    private static Chief instance;

    public static Chief getInstance() {
        if (instance == null) {
            instance = new Chief();
        }
        return instance;
    }

    private Chief() { //конструктор
    }


    @Override
    public void produce(Order order) {

    }
}
