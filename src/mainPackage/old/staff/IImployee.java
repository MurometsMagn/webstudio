package mainPackage.old.staff;

import mainPackage.old.order.Order;

/**
 * Created by Smolentsev Il'ya on 24.01.2020.
 */
public interface IImployee { //OrdinaryEmployee, HeadOfDep, Chief
    void produce(Order order);
}
