package mainPackage.old.staff;

import mainPackage.old.order.Order;

/**
 * Created by Smolentsev Il'ya on 25.01.2020.
 */
public abstract class Imployee implements IImployee { //кроме шефа
    String name;
    public int rank = 0;
    //вероятность выполнения в срок: 70-95%
    double rnd = (Math.random() * (95 - 70)) + 70;

    public boolean execute(Order order) {
        int rnd = (int) (Math.random() * this.rnd) % 2; //выполнил — не выполнил
        if (rnd == 1) {
            System.out.println(order.getOrderTitle() + " выполнен");
            rank++;
            return true;
        } else if (rnd == 0) {
            System.out.println(order.getOrderTitle() + "не выполнен");
            rank--;
            return false;
        } else
            System.out.println("ошибка рендома");
        return true;
    }
}
