package mainPackage.old.staff;

import mainPackage.old.order.Order;

/**
 * Created by Smolentsev Il'ya on 24.01.2020.
 */
public class HeadOfDep extends Imployee {
    private String name;
    Order order;
    //int rank = 0;

    public HeadOfDep(String depName) {
        this.name = depName.concat("DepHeader");
        rank = 0;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    @Override
    public void produce(Order order) {

    }
}
